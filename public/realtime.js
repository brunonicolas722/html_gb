
/*
 * RealTime Database Firebase - PE - EA 2020
 * 0 - https://firebase.google.com/docs/database?authuser=0
 * https://firebase.google.com/docs/database/web/structure-data?authuser=0
 * ¿Base de datos NoSQL? https://es.wikipedia.org/wiki/NoSQL#:~:text=En%20inform%C3%A1tica%2C%20NoSQL%20(a%20veces,como%20lenguaje%20principal%20de%20consultas.
 * 1 - https://firebase.google.com/docs/database/web/start?authuser=0
 * Un detalle importante al momento de la creación de la base de datos. Habilitar de modo de prueba para
 * que cualquier usario puedo modificar la base de datos.
 */
var firebaseConfig = {
  apiKey: "AIzaSyBzoy1Pn10-b6NFzoPLFufMvzGlSvogfs0",
  authDomain: "blog-personal10.firebaseapp.com",
  databaseURL: "https://blog-personal10-default-rtdb.firebaseio.com",
  projectId: "blog-personal10",
};
/* https://firebase.google.com/docs/reference/js/firebase#functions_1
   El valor de retorno del método initializeApp es una instancia de inicialización de la aplicación.
   A partir de aquí podre emplear los servicios disponibles. emoroni */
   var firebase_app = firebase.initializeApp(firebaseConfig);

/*
 ...
*/

// Get a reference to the database service
var database_ref = firebase.database();

   // https://www.w3schools.com/jsref/met_element_addeventlistener.asp
   document.getElementById("boton_escritura").addEventListener('click', escribir_db, false);
   
   /*var valor = 0;
   
   function escribir_db (){
   
     var concept = document.getElementById("concepto").value;
     var imprt = document.getElementById("importe").value;
    
     var ref_nueva = "valor_" + valor;
     valor ++;
     
     firebase.database().ref('Movimientos/' + ref_nueva).set({
       ie: "INGRESO",
       importe: imprt,
       concepto : concept});
   }*/
   
   // 2
   // Create a new post reference with an auto-generated id
   // https://firebase.google.com/docs/database/web/lists-of-data?authuser=0
   // https://firebase.google.com/docs/database/web/lists-of-data?authuser=0#reading_and_writing_lists
   // https://firebase.google.com/docs/reference/js/firebase.database.Reference#push
   // ¿Qué problema existirá al guardar la información de esta manera? emoroni.
   /*function escribir_db (){
   
       var concept = document.getElementById("concepto").value;
       var imprt = document.getElementById("importe").value;
   
       firebase.database().ref('Movimientos/').push({
           ie: "INGRESO",
           importe: imprt,
           concepto : concept
       });
   }*/
   
   var userId;
   
   firebase.auth().onAuthStateChanged(function(user) {
     if (user) {
       // User is signed in.
       userId = firebase.auth().currentUser.uid; // https://firebase.google.com/docs/reference/js/firebase.auth.Auth?authuser=0#currentuser
                                                 // https://firebase.google.com/docs/reference/js/firebase.User?authuser=0#uid
       // console.log("UserID: " + userId);
       child_added(); // Llamo a este método una vez que efectivamente tendo el userID. Si no lo hago así se rompe la ruta. emoroni.
     } else {
       // No user is signed in.
       console.log("No hay un usuario.");
     }
   });
   
   // 3
   // https://firebase.google.com/docs/database/web/lists-of-data?authuser=0#listen_for_child_events
   // https://firebase.google.com/docs/reference/js/firebase.auth.Auth?authuser=0#onauthstatechanged
   // https://www.w3schools.com/jsref/jsref_isnan.asp



   function registrar_rt(){

    var nombre = document.getElementById("nombre").value;
    var apellido = document.getElementById("apellido").value;
    var ciudad = document.getElementById("ciudad").value;
    
    
    if(nombre != '' && apellido != '' && ciudad != '' )
    {
      // console.log("Bandera todo OK"); // Descomentar para debbuing. emoroni
      firebase.database().ref('InicioSesión/' + userId).push({
        ie: "DATOS",
        nombre: nombre,
        apellido : apellido,
        ciudad: ciudad,
      });
    
      document.getElementById("nombre").value = '';
      document.getElementById("apellido").value = '';
      document.getElementById("ciudad").value = '';
      alert('Cuenta creada exitosamente, muchas gracias!');
      
    }
    else 
    {
      // console.log("Bandera campos"); // Descomentar para debbuging. emoroni
      alert('Complete todos los campos por favor.');
    }
    
    }
  
  //var user = firebase.auth().currentUser;
  


   function escribir_db (){
   
     var concepto = document.getElementById("concepto").value;
     //var importe = document.getElementById("importe").value;
   
     //var importe_OK = isNaN(importe);
   
     if(concepto != '')
     {
       // console.log("Bandera todo OK"); // Descomentar para debbuing. emoroni
       firebase.database().ref('Movimientos/' + userId).push({
         ie: "INGRESO",
         //importe: importe,
         concepto : concepto,
         
       });
   
       document.getElementById("concepto").value = '';
       //document.getElementById("importe").value = '';
       alert('Carga exitosa.');
     }
     else if(concepto == '')
     {
       // console.log("Bandera campos"); // Descomentar para debbuging. emoroni
       alert('Complete todos los campos por favor.');
     }
   
   }
   
   // https://firebase.google.com/docs/database/web/lists-of-data?authuser=0#listen_for_child_events
   // Este link puede ser de utilidad - https://stackoverflow.com/questions/20968042/cant-click-button-more-than-one-time-when-append-html-data
   function child_added (){
     firebase.database().ref('Movimientos/' + userId).on('child_added', function(data) {
       var del_id = data.key + "_del";
       var upd_id = data.key + "_upd";
   
       console.log(data.val().concepto);
       //console.log(data.val().importe);
       console.log(data.key);
       console.log(del_id);
       console.log(upd_id);
   
       // https://www.w3schools.com/jquery/default.asp
       // https://www.w3schools.com/jquery/html_append.asp
       $(columna_coleccion).append('<div class="col-sm-4 elementos_listado">' + data.val().concepto + '</div>');
       //$(columna_coleccion).append('<div class="col-sm-4 elementos_listado">' + data.val().importe + '</div>'); 
       $(columna_coleccion).append('<div class="col-sm-4">' + indexar_botones(data.key) + '</div>');
   
       // Aquí existe una diferencia sustancial en la sintaxis cuando es necesario que la función invocada
       // en el click recibe parámetros. emoroni. https://stackoverflow.com/questions/56631140/why-javascript-performs-click-event-when-i-dont-click-on-the-item
       document.getElementById(del_id).addEventListener('click', function () {delete_db('Movimientos/' + userId + '/' + data.key)}, false);
       document.getElementById(upd_id).addEventListener('click', function () {update_db('Movimientos/' + userId + '/' + data.key)}, false);
     });
   }
   
   function indexar_botones (key){
     var boton_borrar = '<button type="button" class="btn btn-danger boton-borrar" id="' + key + '_del">Eliminar</button>';
     var boton_update = '<button type="button" class="btn btn-info boton-update" id="' + key + '_upd">Editar</button>';
     return boton_borrar + boton_update;
   }
   
   function delete_db (path){
     // console.log("Bandera Delete Element"); // Descomentar para debugging. emoroni.
     firebase.database().ref(path).remove();
     alert('Elemento eliminado.');
     location.reload();
   }
   
   // https://www.w3schools.com/js/js_popup.asp
   function update_db (path){
     var txt;
     var concepto = prompt("Cambie su comentario: ", "Update comment");
     //var importe = prompt("Ingrese el importe: ", "Importe");
     
     var update_data = {
       ie: "INGRESO",
       //importe: importe,
       concepto : concepto,
     }
   
     firebase.database().ref(path).update(update_data);
     location.reload();  
   }